#include "../include/mbedaesgcm.h"
#include "mbedtls/gcm.h"

int mbedaesgcm_encrypt(gxByteBuffer* key, gxByteBuffer* iv, gxByteBuffer* aad, gxByteBuffer* plaintext, gxByteBuffer* output, gxByteBuffer* tag)
{
    int ret = 1;

    mbedtls_gcm_context aes_ctx;
    mbedtls_gcm_init( &aes_ctx );
    if( ( ret = mbedtls_gcm_setkey( &aes_ctx, MBEDTLS_CIPHER_ID_AES , key->data, key->size * 8 ) ) != 0 )
    {
        return ret;
    }

    unsigned char output_buf[plaintext->size];
    int tag_len = 12;
    unsigned char tag_buf[tag_len];

    if( ( ret = mbedtls_gcm_crypt_and_tag( &aes_ctx, 
                                            MBEDTLS_GCM_ENCRYPT, 
                                            plaintext->size, 
                                            iv->data, 
                                            iv->size, 
                                            aad->data, 
                                            aad->size, 
                                            plaintext->data, 
                                            output_buf, 
                                            tag_len, 
                                            tag_buf ) ) != 0 )
    {
        return ret;
    }

    bb_set(output, output_buf, plaintext->size);
    bb_set(tag, tag_buf, tag_len);

    mbedtls_gcm_free( &aes_ctx );

    return ret;
}

int mbedaesgcm_decrypt(gxByteBuffer* key, gxByteBuffer* iv, gxByteBuffer* aad, gxByteBuffer* cipheredtext, gxByteBuffer* tag, gxByteBuffer* output)
{
    int ret = 1;

    mbedtls_gcm_context aes_ctx;
    mbedtls_gcm_init( &aes_ctx );
    if( ( ret = mbedtls_gcm_setkey( &aes_ctx, MBEDTLS_CIPHER_ID_AES , key->data, key->size * 8 ) ) != 0 )
    {
        return ret;
    }

    unsigned char output_buf[cipheredtext->size];

    if( ( ret = mbedtls_gcm_auth_decrypt( &aes_ctx, 
                                            cipheredtext->size, 
                                            iv->data, 
                                            iv->size, 
                                            aad->data, 
                                            aad->size, 
                                            tag->data, 
                                            tag->size, 
                                            cipheredtext->data, 
                                            output_buf ) ) != 0 )
    {
        return ret;
    }

    bb_set(output, output_buf, cipheredtext->size);

    mbedtls_gcm_free( &aes_ctx );

    return ret;
}
