#ifndef DLMS_IGNORE_HIGH_SHA256
#include <string.h>
#include "../include/mbedsha256.h"
#include "mbedtls/sha256.h"

int mbedsha256_encrypt(gxByteBuffer* inputData, gxByteBuffer* hashed) {
    int ret = 1;

    unsigned char digest[32];
    ret = mbedtls_sha256_ret( inputData->data, inputData->size, digest, 0 );

    bb_set(hashed, digest, 32);

    return ret;
}

#endif //DLMS_IGNORE_HIGH_SHA256
