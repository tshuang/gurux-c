#include "gxignore.h"

#ifdef  __cplusplus
extern "C" {
#endif


#include "gxbytebuffer.h"

    int mbedecdsa_sign(gxByteBuffer* auth_data, gxByteBuffer* reply_signature);

    int mbedecdsa_verify(gxByteBuffer* auth_data, gxByteBuffer* signature);

#ifdef  __cplusplus
}
#endif
