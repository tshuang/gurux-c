#include "gxignore.h"
#ifndef DLMS_IGNORE_HIGH_SHA256
#ifdef  __cplusplus
extern "C" {
#endif


#include "gxbytebuffer.h"

    int mbedsha256_encrypt(gxByteBuffer* inputData, gxByteBuffer* hashed);
    
#ifdef  __cplusplus
}
#endif
#endif //DLMS_IGNORE_HIGH_SHA256
