#include "gxignore.h"

#ifdef  __cplusplus
extern "C" {
#endif


#include "gxbytebuffer.h"

    int mbedaesgcm_encrypt(gxByteBuffer* key, gxByteBuffer* iv, gxByteBuffer* aad, gxByteBuffer* plaintext, gxByteBuffer* output, gxByteBuffer* tag);

    int mbedaesgcm_decrypt(gxByteBuffer* key, gxByteBuffer* iv, gxByteBuffer* aad, gxByteBuffer* cipheredtext, gxByteBuffer* tag, gxByteBuffer* output);

#ifdef  __cplusplus
}
#endif
